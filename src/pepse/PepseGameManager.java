package pepse;

import danogl.GameManager;
import danogl.GameObject;
import danogl.collisions.Layer;
import danogl.gui.ImageReader;
import danogl.gui.SoundReader;
import danogl.gui.UserInputListener;
import danogl.gui.WindowController;
import danogl.gui.rendering.Camera;
import danogl.util.Vector2;
import pepse.util.CalculusUtils;
import pepse.world.*;
import pepse.world.daynight.Night;
import pepse.world.daynight.Sun;
import pepse.world.daynight.SunHalo;
import pepse.world.trees.Tree;

import java.awt.*;

/**
 * The main class of the simulator.
 */
public class PepseGameManager extends GameManager {
    private static final int TERRAIN_LAYER = Layer.STATIC_OBJECTS;
    private static final int TREE_LAYER = Layer.BACKGROUND + 20;
    private static final int LEAF_LAYER = TREE_LAYER + 1;
    private static final int SUN_LAYER = Layer.BACKGROUND + 1;
    private static final int SUN_HALO_LAYER = Layer.BACKGROUND + 10;
    private static final int NIGHT_LAYER = Layer.FOREGROUND;
    private static final int SKY_LAYER = Layer.BACKGROUND;
    private static final int AVATAR_LAYER = Layer.DEFAULT;
    private static final int DAY_CYCLE_LENGTH = 30;
    private static final Color SUN_HALO_COLOR = new Color(255, 255, 0, 20);

    private Terrain terrain;
    private Tree tree;
    private Camera camera;
    private WindowController windowController;
    private float screenLeftX, screenRightX;
    private int worldBufferSize;

    /**
     * Runs the entire simulation.
     *
     * @param args This argument should not be used.
     */
    public static void main(String[] args) {
        new PepseGameManager().run();
    }

    /**
     * The method will be called once when a GameGUIComponent is created, and again after every invocation
     * of windowController.resetGame().
     *
     * @param imageReader      - Contains a single method: readImage, which reads an image from disk. See its
     *                         documentation for help.
     * @param soundReader      - Contains a single method: readSound, which reads a wav file from disk. See
     *                         its
     *                         documentation for help.
     * @param inputListener    - Contains a single method: isKeyPressed, which returns whether a given key is
     *                         currently pressed by the user or not. See its documentation.
     * @param windowController - Contains an array of helpful, self-explanatory methods concerning the window.
     */
    @Override
    public void initializeGame(ImageReader imageReader,
                               SoundReader soundReader,
                               UserInputListener inputListener,
                               WindowController windowController) {
        super.initializeGame(imageReader, soundReader, inputListener, windowController);
        this.windowController = windowController;
        this.worldBufferSize = (int) windowController.getWindowDimensions().x();
        int seed = 29;

        terrain = new Terrain(gameObjects(),
                TERRAIN_LAYER,
                windowController.getWindowDimensions(), seed);
        tree = new Tree(gameObjects(),
                TREE_LAYER,
                LEAF_LAYER,
                seed,
                terrain::groundHeightAt);

        initializeWorld(imageReader, inputListener, windowController);

        // Sets the X counters for the screen limits
        screenLeftX = camera.screenToWorldCoords(Vector2.ZERO).x();
        screenRightX = camera.screenToWorldCoords(windowController.getWindowDimensions()).x();
        setCollisions();
    }

    /**
     * Sets the collisions of the game objects
     */
    private void setCollisions() {
        // Make the leaves collide with the ground
        gameObjects().layers().shouldLayersCollide(LEAF_LAYER, TERRAIN_LAYER, true);
        // Make the avatar collide with the terrain
        gameObjects().layers().shouldLayersCollide(AVATAR_LAYER, TERRAIN_LAYER, true);
        // Make the avatar collide with the trees
        gameObjects().layers().shouldLayersCollide(AVATAR_LAYER, TREE_LAYER, true);
    }

    /**
     * Initializes the game world
     *
     * @param imageReader      - Contains a single method: readImage, which reads an image from disk. See its
     *                         documentation for help.
     * @param inputListener    - Contains a single method: isKeyPressed, which returns whether a given key is
     *                         currently pressed by the user or not. See its documentation.
     * @param windowController - Contains an array of helpful, self-explanatory methods concerning the window.
     */
    private void initializeWorld(ImageReader imageReader,
                                 UserInputListener inputListener,
                                 WindowController windowController) {
        // Sets the avatar's position to the middle of the world
        float avatarX = windowController.getWindowDimensions().x() / 2;
        float leftAvatarY = terrain.groundHeightAt(CalculusUtils.RoundXToNearestBlock((int) avatarX));
        float rightAvatarY =
                terrain.groundHeightAt(CalculusUtils.RoundXToNearestBlock(
                        (int) avatarX + Avatar.AVATAR_WIDTH / 2));
        // Sets the avatar's height to be the higher between the left and right side of his
        float avatarY = Math.min(leftAvatarY, rightAvatarY);
        Vector2 avatarPosition = new Vector2(avatarX, avatarY - Avatar.AVATAR_HEIGHT);

        // Initialize sky
        Sky.create(gameObjects(), windowController.getWindowDimensions(), SKY_LAYER);

        // Initialize Sun
        GameObject sun = Sun.create(gameObjects(), SUN_LAYER,
                windowController.getWindowDimensions(),
                DAY_CYCLE_LENGTH);

        // Initialize Sun's halo
        GameObject sunHalo = SunHalo.create(gameObjects(),
                SUN_HALO_LAYER,
                sun,
                SUN_HALO_COLOR);
        sunHalo.addComponent(deltaTime -> sunHalo.setCenter(sun.getCenter()));

        // Initialize Night
        Night.create(gameObjects(),
                NIGHT_LAYER,
                windowController.getWindowDimensions(),
                DAY_CYCLE_LENGTH);

        // Create Terrain
        terrain.createInRange(-worldBufferSize,
                (int) windowController.getWindowDimensions().x() + worldBufferSize);

        // Create trees not in the avatar's starting position
        tree.createInRange(-worldBufferSize, (int) avatarX);
        tree.createInRange((int) avatarX + Avatar.AVATAR_WIDTH,
                (int) windowController.getWindowDimensions().x() + worldBufferSize);

        // Creates the avatar
        Avatar avatar = Avatar.create(gameObjects(),
                AVATAR_LAYER,
                avatarPosition,
                inputListener,
                imageReader);

        // Sets the camera to follow the avatar
        camera = new Camera(avatar,
                windowController.getWindowDimensions().mult(0.5f).add(avatarPosition.mult(-1)),
                windowController.getWindowDimensions(),
                windowController.getWindowDimensions());
        setCamera(camera);
    }

    /**
     * Updates the game
     *
     * @param deltaTime time since last update
     */
    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        updateWorld();
    }

    /**
     * Removes all the objects that are out of the world
     *
     * @param layer The layer to remove objects from
     */
    private void removeOutOfWorldObjects(int layer) {
        for (GameObject object : gameObjects().objectsInLayer(layer)) {
            float objectLeftX = object.objectToWorldCoords(Vector2.ZERO).x();
            float objectRightX = object.objectToWorldCoords(new Vector2(1, 0)).x();
            if (objectRightX < screenLeftX - worldBufferSize ||
                    screenRightX + worldBufferSize < objectLeftX) {
                gameObjects().removeGameObject(object, layer);
            }
        }
    }

    /**
     * Updates the world by removing unnecessary objects and creating the needed infinite world
     */
    private void updateWorld() {
        float currentLeftX = camera.screenToWorldCoords(Vector2.ZERO).x();
        float currentRightX = camera.screenToWorldCoords(windowController.getWindowDimensions()).x();

        // Removes all out of world objects
        removeOutOfWorldObjects(TERRAIN_LAYER);
        removeOutOfWorldObjects(TERRAIN_LAYER + 1);
        removeOutOfWorldObjects(TREE_LAYER);
        removeOutOfWorldObjects(LEAF_LAYER);

        // If we need to create the left side of the infinite world
        if (currentLeftX <= screenLeftX - worldBufferSize) {
            terrain.createInRange((int) currentLeftX - worldBufferSize, (int) currentLeftX);
            tree.createInRange((int) currentLeftX - worldBufferSize, (int) currentLeftX);
            screenLeftX = currentLeftX;
            screenRightX = currentRightX;
        }

        // If we need to create the right side of the infinite world
        if (screenRightX + worldBufferSize <= currentRightX) {
            terrain.createInRange((int) currentRightX, (int) currentRightX + worldBufferSize);
            tree.createInRange((int) currentRightX, (int) currentRightX + worldBufferSize);
            screenLeftX = currentLeftX;
            screenRightX = currentRightX;
        }
    }
}
