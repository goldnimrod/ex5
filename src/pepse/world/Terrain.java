package pepse.world;

import danogl.collisions.GameObjectCollection;
import danogl.gui.rendering.RectangleRenderable;
import danogl.util.Vector2;
import pepse.util.ColorSupplier;
import pepse.util.NoiseGenerator;
import pepse.util.CalculusUtils;

import java.awt.*;

/**
 * Responsible for the creation and management of terrain.
 */
public class Terrain {
    public static final String GROUND_TAG = "ground";
    private static final Color BASE_GROUND_COLOR = new Color(212, 123, 74);
    private static final float PERLIN_FACTOR_1 = 1f / 6;
    private static final float PERLIN_FACTOR_2 = 2f / 3;
    private static final int PERLIN_FACTOR_3 = 30;

    private final NoiseGenerator perlinNoise;
    private final GameObjectCollection gameObjects;
    private final int groundLayer;
    private final Vector2 windowDimensions;

    /**
     * Constructor
     *
     * @param gameObjects      The collection of all participating game objects.
     * @param groundLayer      The number of the layer to which the created ground objects should be added.
     * @param windowDimensions The dimensions of the windows.
     * @param seed             A seed for a random number generator.
     */
    public Terrain(GameObjectCollection gameObjects,
                   int groundLayer,
                   Vector2 windowDimensions,
                   int seed) {
        this.gameObjects = gameObjects;
        this.groundLayer = groundLayer;
        this.windowDimensions = windowDimensions;
        this.perlinNoise = new NoiseGenerator(seed);
    }

    /**
     * This method return the ground height at a given location.
     * Uses a implementation of Perlin Noise Algorithm
     * Also normalize the range to be [2/3 * windowDimensions.y, windowDimensions.y]
     *
     * @param x A number
     * @return The ground height at the given location
     */
    public float groundHeightAt(float x) {
        return (float) Math.floor(((perlinNoise.noise(x / PERLIN_FACTOR_3) + 1) * windowDimensions.y()
                * PERLIN_FACTOR_1 + PERLIN_FACTOR_2 * windowDimensions.y()) / Block.SIZE) * Block.SIZE;
    }

    /**
     * This method creates terrain in a given range of x-values.
     *
     * @param minX The lower bound of the given range (will be rounded to a multiple of Block.SIZE)
     * @param maxX The upper bound of the given range (will be rounded to a multiple of Block.SIZE).
     */
    public void createInRange(int minX, int maxX) {
        int startX = CalculusUtils.RoundXToNearestBlock(minX);
        int endX = CalculusUtils.RoundXToNearestBlock(maxX);

        for (int x = startX; x <= endX; x += Block.SIZE) {
            int startY = (int) groundHeightAt(x);
            int endY = (int) (windowDimensions.y() + (1 - PERLIN_FACTOR_2) * windowDimensions.y());
            for (int y = startY; y < endY; y += Block.SIZE) {
                Block block = new Block(new Vector2(x, y),
                        new RectangleRenderable(ColorSupplier.approximateColor(BASE_GROUND_COLOR)));
                // So that all the game objects will only collide with the first 2 blocks of the terrain
                if (y <= startY + Block.SIZE) {
                    gameObjects.addGameObject(block, groundLayer);
                } else {
                    gameObjects.addGameObject(block, groundLayer + 1);
                }
                block.setTag(GROUND_TAG);
            }
        }
    }
}
