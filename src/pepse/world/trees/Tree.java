package pepse.world.trees;

import danogl.collisions.GameObjectCollection;
import danogl.gui.rendering.RectangleRenderable;
import danogl.util.Vector2;
import pepse.util.ColorSupplier;
import pepse.world.Block;
import pepse.util.CalculusUtils;

import java.awt.*;
import java.util.Objects;
import java.util.Random;
import java.util.function.Function;

/**
 * Responsible for the creation and management of trees.
 */
public class Tree {
    private static final int TREE_PERCENT_CREATION = 10;
    private static final int TREE_CREATION_BOUND = 0;
    private static final int LEAF_PERCENT_CREATION = 100;
    private static final int LEAF_CREATION_BOUND = 5;
    private static final int MIN_TREE_HEIGHT = 7;
    private static final int TREE_HEIGHT_RANGE = 7;
    private static final Color TREE_STEM_COLOR = new Color(100, 50, 20);
    private static final String TREE_TAG = "tree";

    private final GameObjectCollection gameObjects;
    private final int treeLayer;
    private final int leafLayer;
    private final int seed;
    private final Function<Float, Float> terrainHeightSupplier;

    /**
     * Construct a new Tree instance.
     *
     * @param gameObjects           The collection of all participating game objects.
     * @param treeLayer             The number of the layer to which the created trees should be added.
     * @param leafLayer             The number of the layer to which the created leaves should be added.
     * @param seed                  A seed for a random number generator.
     * @param terrainHeightSupplier A method which return the ground height at a given location.
     */
    public Tree(GameObjectCollection gameObjects,
                int treeLayer,
                int leafLayer,
                int seed,
                Function<Float, Float> terrainHeightSupplier) {
        this.gameObjects = gameObjects;
        this.treeLayer = treeLayer;
        this.leafLayer = leafLayer;
        this.seed = seed;
        this.terrainHeightSupplier = terrainHeightSupplier;
    }

    /**
     * This method creates trees in a given range of x-values.
     *
     * @param minX - The lower bound of the given range (will be rounded to a multiple of Block.SIZE).
     * @param maxX - The upper bound of the given range (will be rounded to a multiple of Block.SIZE).
     */
    public void createInRange(int minX, int maxX) {
        int startX = CalculusUtils.RoundXToNearestBlock(minX);
        int endX = CalculusUtils.RoundXToNearestBlock(maxX);

        for (int x = startX; x <= endX; x += Block.SIZE) {
            Random random = new Random(Objects.hash(x, seed));
            // 10% chance of creating a tree in this position:
            if (TREE_CREATION_BOUND == random.nextInt(TREE_PERCENT_CREATION)) {
                int groundHeight = Math.round(terrainHeightSupplier.apply((float) x));
                int treeHeight = groundHeight -
                        (MIN_TREE_HEIGHT + random.nextInt(TREE_HEIGHT_RANGE)) * Block.SIZE;
                for (int y = treeHeight; y < groundHeight; y += Block.SIZE) {
                    Block block = new Block(new Vector2(x, y),
                            new RectangleRenderable(ColorSupplier.approximateColor(TREE_STEM_COLOR)));
                    block.setTag(TREE_TAG);
                    gameObjects.addGameObject(block, treeLayer);
                }
                createLeaves(x, treeHeight, random);
            }
        }
    }

    /**
     * Creates the leaves of a single tree
     *
     * @param treeXPos             The x coordinate on screen of the created tree
     * @param treeTopYPos The y coordinate on screen of the created tree
     * @param random        A given random number generator.
     */
    private void createLeaves(int treeXPos, int treeTopYPos, Random random) {
        // create random size for the squared tree-top
        int topSize = 5 + 2 * random.nextInt(2);

        int mostLeftLeaf = treeXPos - (topSize * Block.SIZE / 2);
        int mostRightLeaf = treeXPos + (topSize * Block.SIZE / 2);
        int mostUpLeaf = treeTopYPos - (topSize / 2) * Block.SIZE;
        int mostDownLeaf = treeTopYPos + (topSize / 2) * Block.SIZE;

        for (int i = mostLeftLeaf; i <= mostRightLeaf; i += Block.SIZE) {
            for (int j = mostUpLeaf; j <= mostDownLeaf; j += Block.SIZE) {
                Random leafRandom = new Random(Objects.hash(i, j, seed));
                // 95% chance of creating a leaf:
                if (LEAF_CREATION_BOUND < leafRandom.nextInt(LEAF_PERCENT_CREATION)) {
                    Leaf leaf = new Leaf(new Vector2(i, j), random);
                    gameObjects.addGameObject(leaf, leafLayer);
                }
            }
        }
    }
}
