package pepse.world.trees;

import danogl.GameObject;
import danogl.collisions.Collision;
import danogl.components.ScheduledTask;
import danogl.components.Transition;
import danogl.gui.rendering.RectangleRenderable;
import danogl.util.Vector2;
import pepse.util.ColorSupplier;
import pepse.world.Block;
import pepse.world.Terrain;

import java.awt.*;
import java.util.Random;

/**
 * A class that represents a leaf on a tree
 */
public class Leaf extends Block {
    public static final Color LEAF_BASE_COLOR = new Color(50, 200, 30);
    public static final String LEAF_TAG = "leaf";
    public static final int LEAF_CYCLE_LENGTH = 10;
    private final Vector2 startPosition;
    private final Random random;
    private Transition<Float> horizontalTransition;
    private Transition<Float> spinTransition;
    private Transition<Float> shrinkTransition;

    /**
     * Constructor
     *
     * @param startPosition The leaf's starting position
     * @param random        The random that will be used in the transitions
     */
    public Leaf(Vector2 startPosition, Random random) {
        super(startPosition, new RectangleRenderable(ColorSupplier.approximateColor(LEAF_BASE_COLOR)));
        this.startPosition = startPosition;
        this.random = random;
        this.setTag(LEAF_TAG);
        this.moveLeaf();
        this.leafDropOut();
        this.physics().setMass(0);
    }


    /**
     * Create the leaf's dropping transition
     */
    private void leafDropOut() {
        int startTime = random.nextInt(250);
        new ScheduledTask(this, startTime, false,
                () -> this.renderer().fadeOut(LEAF_CYCLE_LENGTH, this::reset));
        new ScheduledTask(this, startTime, false,
                () -> this.transform().setVelocityY(40));
        new ScheduledTask(this, startTime, false,
                () -> horizontalTransition = new Transition<>(
                        this,
                        velocity -> this.transform().setVelocityX(velocity),
                        -50f,
                        50f,
                        Transition.LINEAR_INTERPOLATOR_FLOAT,
                        2,
                        Transition.TransitionType.TRANSITION_BACK_AND_FORTH,
                        null));
    }

    /**
     * Resets the leaf to its start in the life cycle
     */
    private void reset() {
        this.setVelocity(Vector2.ZERO);
        this.setTopLeftCorner(startPosition);
        this.moveLeaf();
        this.leafDropOut();
        this.renderer().fadeIn(1);
    }

    /**
     * Moves the leaf's from the wind
     */
    private void moveLeaf() {
        new ScheduledTask(this, random.nextFloat(), false,
                () -> spinTransition = new Transition<>(
                        this,
                        angle -> this.renderer().setRenderableAngle(angle),
                        -15f,
                        15f,
                        Transition.LINEAR_INTERPOLATOR_FLOAT,
                        1,
                        Transition.TransitionType.TRANSITION_BACK_AND_FORTH,
                        null));
        new ScheduledTask(this, random.nextFloat(), false,
                () -> shrinkTransition = new Transition<>(
                        this,
                        length -> this.setDimensions(new Vector2(Block.SIZE - length,
                                Block.SIZE)),
                        -5f,
                        5f,
                        Transition.LINEAR_INTERPOLATOR_FLOAT,
                        3,
                        Transition.TransitionType.TRANSITION_BACK_AND_FORTH,
                        null));
    }

    /**
     * Handles a collision of a leaf with the ground
     *
     * @param other     the other object the leaf collided with
     * @param collision The collision object
     */
    @Override
    public void onCollisionEnter(GameObject other, Collision collision) {
        super.onCollisionEnter(other, collision);
        if (other.getTag().equals(Terrain.GROUND_TAG)) {
            this.removeComponent(horizontalTransition);
            this.removeComponent(spinTransition);
            this.removeComponent(shrinkTransition);
            this.setVelocity(Vector2.ZERO);
        }
    }
}
