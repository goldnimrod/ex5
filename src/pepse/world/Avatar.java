package pepse.world;

import danogl.GameObject;
import danogl.collisions.GameObjectCollection;
import danogl.collisions.Layer;
import danogl.components.CoordinateSpace;
import danogl.gui.ImageReader;
import danogl.gui.UserInputListener;
import danogl.gui.rendering.AnimationRenderable;
import danogl.gui.rendering.TextRenderable;
import danogl.util.Vector2;

import java.awt.event.KeyEvent;

/**
 * An avatar can move around the world.
 */
public class Avatar extends GameObject {
    public static final int AVATAR_HEIGHT = 90;
    public static final int AVATAR_WIDTH = 65;
    private static final float VELOCITY_X = 300;
    private static final float VELOCITY_Y = -300;
    private static final float GRAVITY = 300;
    private static final double TIME_BETWEEN_CLIPS = 0.3;
    private static final String[] IDLE_ANIMATION_PATHS = {"pepse/assets/PunkAvatar/idle/1.png",
            "pepse/assets/PunkAvatar/idle/2.png",
            "pepse/assets/PunkAvatar/idle/3.png",
            "pepse/assets/PunkAvatar/idle/4.png"};
    private static final String[] RUN_ANIMATION_PATHS = {"pepse/assets/PunkAvatar/run/1.png",
            "pepse/assets/PunkAvatar/run/2.png",
            "pepse/assets/PunkAvatar/run/3.png",
            "pepse/assets/PunkAvatar/run/4.png",
            "pepse/assets/PunkAvatar/run/5.png",
            "pepse/assets/PunkAvatar/run/6.png"};
    private static final String[] JUMP_ANIMATION_PATHS = {"pepse/assets/PunkAvatar/jump/1.png",
            "pepse/assets/PunkAvatar/jump/2.png",
            "pepse/assets/PunkAvatar/jump/3.png",
            "pepse/assets/PunkAvatar/jump/4.png"};
    private static final String[] FLY_ANIMATION_PATHS = {"pepse/assets/PunkAvatar/fly/1.png",
            "pepse/assets/PunkAvatar/fly/2.png",
            "pepse/assets/PunkAvatar/fly/3.png",
            "pepse/assets/PunkAvatar/fly/4.png",
            "pepse/assets/PunkAvatar/fly/5.png",
            "pepse/assets/PunkAvatar/fly/6.png"};
    private static final int ENERGY_LABEL_SIZE = 40;
    private static final int ENERGY_LABEL_LAYER = Layer.BACKGROUND + 30;

    private final AnimationRenderable idleAnimation;
    private final AnimationRenderable moveAnimation;
    private final AnimationRenderable jumpAnimation;
    private final AnimationRenderable flyAnimation;
    private float energy;
    private final UserInputListener inputListener;
    private final GameObjectCollection gameObjects;
    private GameObject energyLabel;

    /**
     * Construct a new GameObject instance.
     *
     * @param topLeftCorner Position of the object, in window coordinates (pixels).
     *                      Note that (0,0) is the top-left corner of the window.
     * @param dimensions    Width and height in window coordinates.
     * @param inputListener Contains a single method: isKeyPressed, which returns whether a given key is
     *                      currently pressed by the user or not. See its documentation.
     * @param imageReader   Contains a single method: readImage, which reads an image from disk. See its
     *                      documentation for help.
     */
    private Avatar(GameObjectCollection gameObjects,
                   Vector2 topLeftCorner, Vector2 dimensions,
                   UserInputListener inputListener,
                   ImageReader imageReader) {
        super(topLeftCorner, dimensions, new AnimationRenderable(IDLE_ANIMATION_PATHS, imageReader,
                true,
                TIME_BETWEEN_CLIPS));
        this.gameObjects = gameObjects;
        this.energy = 100;
        this.inputListener = inputListener;
        idleAnimation = new AnimationRenderable(IDLE_ANIMATION_PATHS,
                imageReader,
                true,
                TIME_BETWEEN_CLIPS);
        moveAnimation = new AnimationRenderable(RUN_ANIMATION_PATHS,
                imageReader,
                true,
                TIME_BETWEEN_CLIPS);
        jumpAnimation = new AnimationRenderable(JUMP_ANIMATION_PATHS,
                imageReader,
                true,
                TIME_BETWEEN_CLIPS);
        flyAnimation = new AnimationRenderable(FLY_ANIMATION_PATHS,
                imageReader,
                true,
                TIME_BETWEEN_CLIPS);
        physics().preventIntersectionsFromDirection(Vector2.ZERO);
        transform().setAccelerationY(GRAVITY);
        createEnergyLabel();
    }

    /**
     * Creates the Avatar's energy label
     */
    private void createEnergyLabel() {
        TextRenderable energyText = new TextRenderable("");
        energyLabel = new GameObject(Vector2.ZERO,
                new Vector2(ENERGY_LABEL_SIZE, ENERGY_LABEL_SIZE),
                energyText);
        energyLabel.setCoordinateSpace(CoordinateSpace.CAMERA_COORDINATES);
        gameObjects.addGameObject(energyLabel, ENERGY_LABEL_LAYER);
    }

    /**
     * This function creates an avatar that can travel the world and is followed by the camera.
     * The can stand, walk, jump and fly, and never reaches the end of the world.
     *
     * @param gameObjects   The collection of all participating game objects.
     * @param layer         The number of the layer to which the created avatar should be added.
     * @param topLeftCorner The location of the top-left corner of the created avatar.
     * @param inputListener Used for reading input from the user.
     * @param imageReader   Used for reading images from disk or from within a jar.
     * @return A newly created representing the avatar.
     */
    public static Avatar create(GameObjectCollection gameObjects,
                                int layer, Vector2 topLeftCorner,
                                UserInputListener inputListener,
                                ImageReader imageReader) {
        Avatar avatar = new Avatar(gameObjects,
                topLeftCorner,
                new Vector2(AVATAR_WIDTH, AVATAR_HEIGHT),
                inputListener,
                imageReader);
        gameObjects.addGameObject(avatar, layer);
        return avatar;
    }

    /**
     * Updates the avatar according to the user's input
     *
     * @param deltaTime Time since last update
     */
    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        float xVel = 0;
        // Move Left
        if (inputListener.isKeyPressed(KeyEvent.VK_LEFT)) {
            xVel -= VELOCITY_X;
            this.renderer().setRenderable(moveAnimation);
            this.renderer().setIsFlippedHorizontally(true);
        }
        // Move Right
        if (inputListener.isKeyPressed(KeyEvent.VK_RIGHT)) {
            xVel += VELOCITY_X;
            this.renderer().setRenderable(moveAnimation);
            this.renderer().setIsFlippedHorizontally(false);
        }
        // Jump only if avatar is on standing
        if (inputListener.isKeyPressed(KeyEvent.VK_SPACE) && getVelocity().y() == 0) {
            transform().setVelocityY(VELOCITY_Y);
            this.renderer().setRenderable(jumpAnimation);
        }
        // Fly
        if (inputListener.isKeyPressed(KeyEvent.VK_SPACE) &&
                inputListener.isKeyPressed(KeyEvent.VK_SHIFT)) {
            fly();
        }
        // Idle
        if (getVelocity().y() == 0) {
            increaseEnergy();
            if (xVel == 0) {
                this.renderer().setRenderable(idleAnimation);
            }
        }
        transform().setVelocityX(xVel);
        updateEnergyLabelText();
    }

    /**
     * Updates the text of the energy label according to the avatar's current energy
     */
    private void updateEnergyLabelText() {
        ((TextRenderable) energyLabel.renderer().getRenderable()).setString("Energy: " + energy);
    }

    /**
     * Let the avatar fly, if he has energy left
     */
    private void fly() {
        if (this.energy > 0) {
            transform().setVelocityY(VELOCITY_Y);
            this.energy -= 0.5f;
            this.renderer().setRenderable(flyAnimation);
        }
    }

    /**
     * Increases the avatar's energy
     */
    private void increaseEnergy() {
        if (this.energy < 100)
            this.energy += 0.5f;
    }
}
