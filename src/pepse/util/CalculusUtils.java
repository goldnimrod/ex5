package pepse.util;

import pepse.world.Block;

/**
 * A class for calculation utils
 */
public class CalculusUtils {
    /**
     * Rounds the current x value to the value of the nearest Block
     *
     * @param x The X value
     * @return The nearest Block's X value
     */
    static public int RoundXToNearestBlock(int x) {
        if (x % Block.SIZE != 0) {
            if (x > 0) {
                return (x / Block.SIZE) * Block.SIZE;
            } else {
                return ((x / Block.SIZE) - 1) * Block.SIZE;
            }
        }
        return x;
    }
}
